## GUADEC 2020 Slides

```
├── accessibility
│   ├── gtk-accessibility.md
│   └── notes.md
├── dependencies.txt
└── maintainers
    ├── maintainers.md
    └── notes.md
```

 - `accessibility`: Archaeology of Accessibility
 - `maintainers`: Being a GNOME maintainer
 - `dependencies.txt`: List of dependencies for Fedora

### License and copyright

Copyright 2020  Emmanuele Bassi

Released under the terms of the CC by-sa 4.0 license
