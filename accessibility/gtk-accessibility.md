% Archaeology of Accessibility
% A look at the past and future of the Accessibility stack
% Emmanuele Bassi (`ebassi@gnome.org`)

---

> - Emmanuele Bassi
> - He/him
> - GNOME Foundation
> - GTK

---

# Let's talk about accessibility

---

*Accessibility*, n.: The design of products, devices, services, or environments so as to be usable by people with disabilities.

---

*Assistive technology*: Assistive, adaptive, and rehabilitative devices for people with disabilities or the elderly population.

---

# Who benefits from accessibility?

::: notes

There's a common misconception about the topic of "accessibility". When
people say that an application, a document, or a web page must be
accessible, your mind goes to certain images.

And, yes: making an application accessible is definitely required for a
certain kind of specialised input and output devices; but the most common
consumer of an assistive technology are these two people.

:::

---

![© Frankie Fouganthin, CC SA-4.0](stephen-hawking.jpg)

---

![© Universal Studios](sneakers.jpg)

---

![&nbsp;](parents.jpg)

::: notes

These are my parents. They are in their late 60s, and are fairly active,
going on holidays on a motorbike, or visiting countries on the other side of
the planet. You can say that they are both computer literate: back when I
was a teenager, my mother used to type faster than I did on a computer
keyboard; and my father is the reason I'm a nerd that likes programming.
Nevertheless, these days they need a lot more affordances than I do, when it
comes to using computers and phones.

:::

---

## Who benefits from accessibility?

We all do.

::: notes

I am 39 years old. I have been wearing glasses for the past 25 years. For
more than that, I have been listening to music of varying levels of
intensity with a vast assortment of headphones. Since I've started working
as a professional software developer, I've had to improve my posture and
switch to different input devices to avoid repetitive stress injuries, and I
still have days where my ability to use a pointing device is severely
limited.

Let's face it: none of us are getting any younger, and if you are in your
teens or twenties, **this** is as good as your body gets. The days where you
can keep shoving tons of 12 by 12 pixels widgets on screen are not going to
last. It's all downhill from now on.

So, *we are the next batch of consumers of accessible software*, whether we
like it or not. We owe it to everyone, including our future selves, to make
it work.

:::

---

## Some history

> - GTK 1.3 → 2.0
> - ATK
> - AT-SPI
> - GAIL

::: notes

Accessibility support landed in GTK during the 1.3 development cycle, which
led to GTK 2, released in 2002; it was the contribution of the Sun
Accessibility team, and it consisted of three components:

- ATK, a series of abstract types and interfaces, meant to be implemented by
  GObject-based toolkits
- AT-SPI, a protocol designed to let applications and assistive technologies
  talk to each other
- GAIL, a GTK module that turned ATK data types into AT-SPI data types,
  bridged the two worlds

The charitable reason for this design was that Sun already had an
[accessibility API for Java][java-a11y], and it made sense to try and write
something that worked in a similar way; in reality, it seems that very few
people knew *why* the Java accessibility API worked they way it did, and it
was rather easier to copy it wholesale than change it. There's nothing that
screams "dot-com bubble" like having an Hypertext *and* an Hyperlink classes
in your API.

Additionally, the reason why the whole thing worked off of a GTK module,
instead of being implemented inside the toolkit itself, was that the
communication channel was using [CORBA][corba]. If you are young enough not
to know what CORBA is, imagine the enterprise version of any reasonable IPC
mechanism, then make it even more corporate, and design it so that it
requires a centralised body to assign names, because *of course* this thing
must work on a distributed network. Once upon a time, the whole of GNOME
worked like that—that's where the whole "network object model" thing comes
from. Since the CORBA implementation was not going to end up in the toolkit,
the accessibility stack had to live out of tree, like a theme engine or an
input method, and was subjected to pretty much the same amount of design and
scrutiny.

But, hey: it allowed some product manager to check out a box, so it's
impossible to say if it's bad or not.
:::

---

# Accessibility is **not** a checkbox

::: notes

Accessibility, though, is a process, not something you check once and then
forget about—and we ended up forgetting about it.

:::

---

## The world has changed

> - CORBA → DBus
> - Out of tree modules → built in functionaliy
> - X11 → Wayland
> - Sandboxing

::: notes

For the following 18 years, this was the state of our accessibility stack.
With GNOME 3 we managed port AT-SPI away from CORBA and to DBus (more on
that later), and we moved the code from a module in GTK to a library that
the toolkit can link against, but the architecture is exactly the same.

In the meantime, we went from X11 to Wayland; from applications installed in
a global flat namespace, with implicit access to everything on the user's
system, to sandboxed applications with negotiated access to resources and
explicit user consent.

:::

---

# Accessibility in GNOME needs to change too

::: notes

And, yet, accessibility has been stuck. It still thinks we are in an X11
world, where applications have access to all devices, and can randomly be
poked from other applications; it still uses DBus like it would use CORBA,
which means: not really efficiently. In order to avoid the accessibility
stack from slowing your whole desktop down to a crawl, by broadcasting a ton
of small parcels of data every time your pointer moves, we had to move it to
a separate connection—which is available to everyone for snooping. In the
meantime, not many people are left that can work on the stack, because not
many companies can afford to set up a team to work on it, and volunteer work
will only ever do so much. Imagine you find something missing in your
favourite free software project, and you decide to "scratch your own itch",
so to speak; now imagine not being able to literally access the project—not
the source code, not the tools, not the end result. If you can't perceive
it, it does not exist, which means: you can't affect any change to it.
That's why asking users of assistive technologies: "is there any regression
in my application" or "is there a missing feature" is pointless. If you
cannot perceive a feature, it does not exist.

How do we change this?

:::

---

## How do we change it?

> - Consolidate the effort
> - Simplify the toolkit
> - Empower application developers
> - Funding

::: notes

From a technological standpoint, we need to consolidate the effort. We
currently have a bunch of API in GTK that hasn't been touched in nearly 20
years, even though is passed through a couple of major releases. We have an
abstraction layer that is not really abstracting anything, because every
time we need to add things, we end up adding it three times: once in the IPC
interface; once in the abstraction layer; and once in the toolkit.

From a resource standpoint, we need to invest back into accessibility in
both the core platform and the application development process. Application
developers do not really care about this enough, and are convinced that GTK
will deal with everything, which is hilariously untrue. Of course, even if
they did care about it, they'd be confronted with a pretty terrible API that
is basically undocumented, and hard to understand. There are no tools or
documentation for helping application authors, and guide them into making
their own widgets accessible.

We are also going to need funding, and help from sponsors. We cannot ask
people using the accessibility stack to also write it, so we need to have
abled programmers, designers, and testers working on it.

:::

---

## New accessibility stack

::: notes

Thanks to the GNOME Foundation, I've been working on the GTK accessibility
API for the past six months; I have been trying to minimise the API needed
for application developers, and reintegrate most of what was abstracted for
the benefit of a platform that doesn't really exist any more into GTK
proper. You don't need to look at ATK, any more: all that stuff is gone.

:::

---

## WAI-ARIA

> - [W3C standard](https://w3c.github.io/aria/)
> - **element**: an accessible part of the UI
> - **role**: what the element *does*
> - **attributes**: what the element *has*
>   - property
>   - relation
>   - state

::: notes

We still want a certain abstraction in order to be portable to other
platforms, but that abstraction does not need to be an API. Since we've
been using the Web for our drawing model, it makes sense to use the Web for
our accessibility stack as well. The W3C has a specification called
[ARIA][wai-aria] which defines the onthology of accessible roles—what a UI
component does—and the list of properties, states, and relationships that
each role has. We had a similar set of information in ATK, but by using a
standardised approach we can rely on shared knowledge and, possibly,
tooling.

:::

---

## Toolkit/1

> - **element**: `GtkAccessible`
> - **role**: `GtkAccessibleRole`
> - **attributes**
>   - `GtkAccessibleProperty`
>   - `GtkAccessibleRelation`
>   - `GtkAccessibleState`
> - **platform-specific backend**: `GtkATContext`

::: notes

The new API tries to be very simple:

 - you have an interface for accessible objects
 - each accessible object has a role, and each role has a set of states,
   properties, and relations
 - every time any state, property, or relation is changed, we notify the
   underlying platform accessibility API; on Linux, this would be AT-SPI

From a toolkit perspective, our job is to provide application developers
with an API that can be used when creating your UI, and especially when
creating your own composite widgets. This API must be documented, and
tested.

:::

---

## Toolkit/2

Set the role on your widget

```c
// GtkCheckBox: set role

gtk_widget_class_set_accessible_role (
  widget_class,
  GTK_ACCESSIBLE_ROLE_CHECKBOX
);
```

---

## Toolkit/3

Change states

```c
// GtkCheckButton: state change

gtk_accessible_update_state (
  GTK_ACCESSIBLE (check_button),
  GTK_ACCESSIBLE_STATE_PRESSED, is_active,
  -1
);
```
---

## Toolkit/3

Change properties/1

```c
// GtkEntry → accessible label

gtk_accessible_update_property (
  GTK_ACCESSIBLE (username_entry),
  GTK_ACCESSIBLE_PROPERTY_LABEL, "User name",
  -1
);
```

---

## Toolkit/3
 
Change properties/2

```c
// GtkRange: change adjustment

gtk_accessible_update_property (
  GTK_ACCESSIBLE (scroll_bar),
  GTK_ACCESSIBLE_PROPERTY_VALUE_MIN, gtk_adjustment_get_lower (adj),
  GTK_ACCESSIBLE_PROPERTY_VALUE_MAX, gtk_adjustment_get_upper (adj),
  GTK_ACCESSIBLE_PROPERTY_VALUE_NOW, gtk_adjustment_get_value (adj),
  -1
);
```

---

## Toolkit/4

Change relations

```c
// GtkPasswordEntry → GtkLabel

gtk_accessible_update_relation (
  GTK_ACCESSIBLE (password_entry),
  GTK_ACCESSIBLE_RELATION_LABELLED_BY, password_label,
  -1
);
```

---

## Applications/1

> - Follow the [rules](https://www.w3.org/TR/using-aria/)

---

## Five Rules of ARIA

> - 1st: Reuse existing accessible UI elements
> - 2nd: Do not change semantics
> - 3rd: Keyboard accessibility
> - 4th: Do not hide focusable elements
> - 5th: Add an accessible label

::: notes

From an application development perspective, authors must follow the Five
Rules of ARIA:

 - if there's a UI element that already has semantics and behaviours you
   want, use it instead of creating your own and adding accessible roles,
   states, and properties
 - do not change the semantics of a UI element through accessibility; do
   not add button semantics to a purely presentational UI element
 - all interactive UI elements should be accessible through the keyboard;
   don't limit interaction to touch or pointing devices
 - do not hide a focusable UI element from the accessibility stack; if the
   user can interact with it, it should be accessible
 - all interactive UI elements must have an accessible name; either use the
   actual "label" property, or set the "labelled by" relation

:::

---

## Applications/2

> - `gtk-builder-tool`
> - `GtkTestATContext`
> - `gtk_test_accessible_assert_*`

::: notes

Ideally, we should have tools that can be used in our CI pipeline, and that
can be used to verify that, for instance, all your entry widgets either have
an accessible label, or are related to a label widget that describes them.
We should also give you an API that you can use to write tests for your
custom widgets, so that you can verify that toggling a button, or activating
a switch, will emit the correct accessible state.

:::

---

## Current state

> - Finalising the API
> - Porting GTK widgets
> - Writing tests
> - Documentation
> - Implementing the AT-SPI backend

::: notes

The current state of this work is available on GitLab; we're finalising the
API as we speak, and porting all the GTK widgets to it. I'm also writing
tests and the AT-SPI implementation, so that we can release GTK4 with a
working backend on Linux.

:::

---

## How can you help?

> - Help writing tests
> - Documentation
> - Port to Windows/macOS
> - Improve access for sandboxed applications
> - Funding

::: notes

How can you help?

We can definitely get help in writing tests, porting widgets, and generally
validating the API and implementation. Help in writing the documentation is
always welcome. If you are familiar with the accessibility stack on Windows
and macOS, and want to help us with writing and testing the backend there,
you'd be a personal hero of mine.

If you work in other environments that consume the same underlying
accessibility stack as we do, we also want to talk to you for standardising
access to AT-SPI for sandboxed applications; we have plans for that, once we
clean up the messy swamp that is the current stack, and we are going to need
a proper cross-desktop environment solution.

This effort isn't really cheap, by the way, so funding is also really
welcome: once we have released GTK4 with the new API, we're also going to
maintain it, and that means having resources to do so for the foreseeable
future.

:::

---

## Shout out to

> - GNOME Accessibility team
> - GNOME Foundation
> - Hypra

::: notes

Finally, I'd like to thanks a few people:

 - first of all, the GNOME Accessibility team, for keeping the torch lit,
   and ensuring that people could continue using GNOME through the
   accessibility stack; they are all heroes, each and every one of them
 - the GNOME Foundation, for kindly employing me to work on GTK
 - [Hypra][hypra], who work on accessibility in free software, and came
   to the GTK hackfest in January, before all the mess with COVID-19
   started, to try and plan the next phase of the accessibility stack

:::

---

# Questions?

---

# Thanks!
