% GLib/GTK Birds of a Feather Session
% Agenda

---

## GTK

 - GTK4 readiness
 - 3.99
 - Post-4.0

---

## GTK4 readiness

 - Unfinished tasks
   - a11y api
   - device cleanup
   - OS X backend
   - File chooser port ?
 - Pushed off
   - Animation api
 - Dependencies
   - Need a cairo release for subpixel positioning
   - Some unmerged GLib changes would be nice (interface binding, standard::file)
 - Reverse dependencies
   - gtkwebkit
   - gtksourceview
   - vte
   - libhandy
   - libgweather
   - ...
 - Other: docs, demos...

---

## 3.99

 - to be done as soon as a11y lands
 - will have a "port and try now!" post
 - plan to do at least 1 or 2 snapshots between 3.99 and 4.0

---

## Post 4.0 plans

 - Animation api ?
 - Killing cell renderers ?
 - Killing style contexts ?
 - css optimizations (unboxing, etc...)
 - Do some of our list models belong into glib ? (The answer seems no)
 - Thread all the things, immutable data structures
 - Bring in libhandy things

---

# GLib

---

 - do we want to do `gint` -> `int` style cleanups?
   - can't fix `gboolean`, although `gboolean` -> `int` would be possible, and we can have a `g_param_spec_bool`
 - `GUri`
   - regressions, scope, API and go/no-go before API freeze
   - Vs https://github.com/WebKit/webkit/blob/master/Source/WTF/wtf/URLParser.h and https://github.com/WebKit/webkit/blob/master/Source/WTF/wtf/URL.h#L225
 - Features needed for GTK 4
 - `g_file_set_contents()` atomicity [issue](https://gitlab.gnome.org/GNOME/glib/-/merge_requests/369)
 - Project governance/maintenance/new contributors discussion 
