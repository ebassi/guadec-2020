% Being a GNOME maintainer
% Best practices and known traps for newcomers and seasoned veterans
% Emmanuele Bassi (`ebassi@gnome.org`)

---

> - Emmanuele Bassi
> - He/him
> - GNOME Foundation
> - GTK
> - Maintained a bunch of projects

---

## Disclaimer

::: notes

This presentation is not going to be technical. I am not going to spend time
teaching you how to use GitLab, or how to make release tarballs, or how to
upload documentation to the GNOME infrastructure. That stuff is transitory,
and doesn't really matter.

It's also not going to be a talk in the form of list of 12 things you don't
know about being a GNOME maintainer, and the 7th one will surprise you.

Most of the information in the presentation is pretty much common knowledge,
but it's common knowledge that we currently require people to learn through
the years, instead of telling them upfront.

:::

---

# Part I: My Shot

::: notes

My road to maintaining GNOME modules started when I offered to help fixing
the Dictionary panel applet, at the time part of the gnome-utils repository.
The GNOME utilities were a project that dated back to the pre-1.0 days of
GNOME, so it had a long history of contributors and maintainers. Fixing the
Dictionary equired a complete rewrite of the existing code in the
application, and then reusing the code in the applet. At the time, I was
mostly contributing to the Perl bindings for GNOME libraries, and I had
started writing the recently used files API in GTK, so this was my first
foray into writing actual application code. After six months, I landed the
new Dictionary application, and I started doing releases when the original
maintainer sent me an email saying he was in the middle of a move from the
US to Europe, and would I be so kind to spin the distribution archives for
GNOME 2.12? Of course, I've never heard back from him again. The well known
"tag, you're it" model of maintainership transfer.

The path of each one of us toward maintenance is different.

This is my personal journey, and these are my personal recommendations. I
don't pretend to be the only source of truth in how to be a maintainer; all
I have to offer is 15 years of involvement in the community, a bunch of
software releases, some scars from flames, and a deep and long lasting
cynicism about software development, especially in free software.

Do what thou wilt with this knowledge.

Since this is presentation is also the result of my personal journey, I
reserve the right to contradict myself, and blatantly ignore the
recommendations I make here. What can I say: I am a complicated man. Again
you should feel free to pick and choose what you think it's best for you.
I can only serve as a warning.

:::

---

## GNOME

![© Jonathan Blandford](gnome-coat-of-arms.png){width=50%}

::: notes

If you've seen Jonathan Blandford's presentation in 2017 about the history
of GNOME, you will be familiar with GNOME's motto, "GNOME is people".

There's another thing made out of people…

:::

---

## GNOME is People

![© Metro Goldwyn Meyer](soylent-green.jpg){width=75%}

::: notes

Like Soylent Green, maintaining a free software project will take your body,
mulch it down until it's a paste to be consumed by somebody else, leaving only
your hollowed husk behind.

Incidentally, I apologise for spoiling the ending of a 47 years old movie; to
be fair, the trailer for the movie spoiled the ending as well. The '70s were
weird, man.

Anyway.

Unlike Soylent Green, GNOME is also made of two other components:

- software; which is written, designed, documented, translated, and tested
  by the aforementioned "people"
- processes; which are followed by the same people in order to produce the
  software that can be consumed

So, if we put "people" at one end of the spectrum, and "software" at the
other, we get a nice little diagram on how to turn people into software.

:::

---

## Processes

![Transforming People into Software](process.png){width=75%}

::: notes

We, justly, celebrate the people that make the software; and of course we
also celebrate our software in the form of releases; but the bits and blobs
in the middle are kind of obscure—often so obscure that people involved in
the GNOME project discover them long after they joined it.

The objective of this talk is to present the processes, including the bear
traps cleverly disguised in the tall grass, and possibly spurn people into
figuring out better processes—but, most importantly, to improve the
communication across the whole project.

Maintaining software is a thankless job; I've learned a bunch of these lessons
the hard way, in some cases. Back when I was dropped in front of a GNOME
module, we only had a wiki page outlining the process to make a release and
publish it; everything else, I learned by observing other maintainers; or I
had to ask; or I had to experience it personally.

:::

---

# Part II: The room where it happens

::: notes

Maintaining free software projects is not simple.

It typically involves a single person doing the body of work of the
equivalent of a team in the proprietary, or in the commercial world:
development, QA, client feedback, infrastructure, marketing, documentation
writing, evangelism.

It's a lot of work for a team. It's an incredible amount of work for a
single person.

:::

---

## I guess I’m gonna fin’ly have to listen to you

 - You want to be a developer, stick to coding
 - You want to be a maintainer, you get to deal with people

::: notes

You can decide to stick to coding, but then you're just that: a developer.
It's perfectly fine. Lots of free and open source software project are just
that: a single person, doing something they want, until they either burn out
or they move on to something else.

Or you can involve different people in the effort, but then you become a team
lead, and you get to manage people more than your project.

:::

---

## Talk less, smile more

 - Code is less important than people
 - People scale less than code
 - You cannot maintain a large project by yourself

::: notes

The important thing to understand about maintaining software is that it's
not about software at all. Software is simple. Software is easy.

People. People are far, far from easy. They don't scale, for a start. You'll
always, always have fewer people than the ones you need to match the scope
of the project you have.

And, most importantly, when you're alone at maintaining a project, you are
now the component that doesn't scale.

:::

---

## Well, hate the sin, love the sinner

> - Users don't know what they want
> - Users don't know how things work
> - Users don't know how to give feedback
> - Users are fundamental
>   - source of new contributors
>   - source of motivation
>   - source of demotivation
> - You are a user too

::: notes

These are some fundamental truths about users; and about you.

Because unless you write software that you never use, like a free and open
source software to run a nuclear power station, you are going to directly use
the thing you write. That's why you wrote the thing in the first place!

:::

---

## Decisions are happening over dinner

> - High bandwidth, low latency
> - Chat (IRC, Matrix, whatever)
> - Video conference
> - ~~Hackfests~~
> - ~~Conferences~~
> - Remember when those were a thing?

::: notes

Once you start involving other people, in any role, you'll need to have
access to high bandwidth/low latency communication channels. Anything else
will not scale, unless your project is very, very slow moving.

:::

---

## No one really knows how the game is played

 - Text files in the repository?
 - Wikis?
 - Mailing list archives?
 - Institutional knowledge passed down by community elders around the fire at sundown?
 - A basic micro-pulse to the brain, and teleologically experiencing the whole of the GNOME development community memories?

::: notes

Another thing you will need once you involve other people, is a way to
transfer knowledge from your head into theirs. As they say where I'm from,
"nobody is born already learned". You had to learn, so you should not be
at all shocked if somebody else needs to do the same.

We don't have good ways to transfer knowledge, in the free software community.
Sure, we can dump a bunch of files in a source code repository, or write down
some wiki page, but everything tends to go out of date, for the same reason
why writing a book against a free software library is a sisyphean task: things
change, often too fast, for documentation to keep up. Even best practices
change. On the infrastructure side, twenty years ago we were using CVS to
store our code, and Bugzilla to track issues; ten years ago we had just moved
away from Subversion and we were either using Git or Bazaar. Five years ago we
didn't have GitLab. Maybe—hopefully—in five years time we're still going to
use it, but it's likely going to look and behave differently than today.

:::

---

## The art of the trade

 - Chat
 - Discourse
 - Wiki
 - Foundation sponsoring for hackfests

::: notes

GNOME offers a bunch of services to projects hosted on our infrastructure.
Don't try to reinvent your own; and don't try to go at it alone. Don't decide
to use your own chat service. Don't create a random mailing list.

By using shared services, you let other people in the project reach you.

:::

---

## How the sausage gets made

 - Take your time to teach other people
 - Write down all that you know
 - Mark issues for newcomers, and outline a solution *that will get merged*
 - Ask newcomers to review merge requests

::: notes

The only way to help new contributors get up to speed is training, in the
form of mentorship.

You, as a maintainer, are responsible for teaching new contributors how to
do your job, and bring them up to speed. This, of course, means less time
for coding.

Don't wait until Google or Outreachy or whoever else comes around and pays
a bunch of people to be your interns for a season, before you write down what
an intern should know, and do. Be proactive.

:::

---

## We just assume that it happens

 - Design
 - CI Pipeline
 - Localisation
 - Documentation
 - Nightly builds

::: notes

A lot of stuff is needed to make software happen. From the outside, all of
these things happen by sheer magic, like elves appearing at night, making
shoes for the shoemaker to find in the morning.

Nothing could be further from the truth.

Each and every one of these things happen because somebody is putting time,
effort, blood, sweat, and tears. They time is not in any way, shape, or form
less precious than yours. Their motivations are not any more spurious, their
committment is not any less complete than yours. They are experts in their
fields, just like you're an expert on how to string together two statements
and a condition, and running a compiler.

:::

---

## I arranged the menu, the venue, the seating

 - GNOME infrastructure
   - Chat/Discourse
   - GitLab
   - Damned Lies
   - `help.gnome.org`
   - gnome-build-meta

::: notes

The GNOME project gives all of the people above something for to allow them
to do their job at the best of their abilities.


:::

---

## No one really knows how the parties get to yes

 - Design team
 - Release team
 - Code of conduct team
 - Foundation board

::: notes

GNOME also has a bunch of people dedicated to ensuring that the people
involved talk to each other, and resolve their differences in a constructive
way.

Figure out how to talk to each of them. Keep them in the loop, before and
after you've written down code.

:::

---

## The pieces that are sacrificed in evʼry game of chess

 - You don't get to be your unique and precious snowflake
 - Ask the design team before you go off the deep end
 - Involve the localisation team for user visible text
 - Talk to the release team about new dependencies
 - Talk to the maintainers of modules you depend on, as well as the ones that depend on you
 - You are part of a community; you don't work in a vacuum

::: notes

We all sacrifice something of our individuality when we work together. But
what we gain is greater than what we lose. We give up the sense of power and
importance in maintaining our little fiefdom, where every decision we make is
ours and ours alone, and where our word is the law; in return, we get help,
knowledge, and a shared sense of purpose. This is what "being a community"
means.

The important lesson working in GNOME teaches you is that it's not all about
you. Not the software you write, not the API you provide, not the UI you
design. Of course, it doesn't mean you write software for somebody else: you
are a user, too; but you have to think of other people as well.

:::

---

## Not every issue can be settled by committee

 - Rough consensus
 - Steering committee
 - (SA)BDFL
 - User feedback
 - At the end of the day, you have to make a decision

::: notes

There are other ways to deal with conflict, and with decision making, rather
than having maintainers duke it out among themselves.

Some of these alternatives have been used by GNOME; some have been used by
other projects. Some of these have worked, while for others the jury is
still out.

At the end of the day, though, you still have to arrive at a decision.

:::

---

## It isnʼt pretty

> - Flames
> - Character assassinations over Planet GNOME
> - Passive aggressive emails to other projects
> - Revert wars
> - Forks
> - Publicly leaving the project with grand theatrical gestures

::: notes

We are a free and open source software project, we have had everything, and
more, on this list.

Because if there's something free software projects like more than anything
is a circular firing squad.

:::

---

## A *quid pro quo*

 - You can't always get what you want

::: notes

Assuming you don't want to burn your bridges, and leave the project, you
will have to compromise; try to agree on a minimal platform, and build from
there.

:::

---

## Well, weʼll see how it goes

 - But if you try sometimes well you might find you get what you need

::: notes

In many cases, it's just some miscommunication. Take advantage of the
resources GNOME provides to explain your issue, and listen to other parties.
You might end up resolving the problem more quickly, or figure out a
different approach. Maybe even a better one! Stranger things have happened.

:::

---

## Weʼll never really know what got discussed

![My typical reaction when people bring up tray icons](unremarkably-awful.png){width=75%}

::: notes

Some times you'll propose a thing that has already been proposed a thousand
times. It happens. You're not as clever as you think you are; I know I am
not. Neither are other maintainers, but they have probably been on the
receiving end of the same stuff over and over again.

Don't pretend everyone drop what they're doing to answer your question, and
don't make up fantastical stories based on half-heard rumours. Just ask for
information; you'll likely get one, or at least you'll be pointed in the
direction of one.

:::

---

## Click-boom then it happened

 - Decisions happen during a discussion
 - Some discussions go on for *years*
 - Reviews, issues, chat, hackfests
 - Some are logged, some aren't

::: notes

Not every decision is, or can be notarised. In some cases, it's the result
of a long discussion that has been going on for years. In some cases, it was
taken in a very different context or venue, and we don't go around with a
microphone recording everything that happens.

Nevertheless, maintainers should put some time into writing down the common
decisions; it saves time in the long run. If Fate selected you to repeat
yourself every month, on every platform in existence, about the same things,
then keep the links handy, and write them down in a document. Nobody will
read them at first, because: of course they won't; but it's easier for you
to point at them, and not waste two hours writing down the same stuff, making
your blood pressure skyrocket, and burning you out.

:::

---

## When you got skin in the game you stay in the game

 - Group maintenance
 - Trading reviews
 - Personal responsibility

::: notes

Share the ownership of what you maintain.

People care more if they feel responsible for something. Use the natural
sense of belonging to an in-group to herd cats towards a direction, instead
of having everyone pull along different vectors. 

The Shell developers are great at this: they trade reviews, they have clear
maintenance roles, and they have a shared sense of ownership of the project;
all of this enables new contributors to hit the ground running.

:::

---

## But you donʼt get a win unless you play in the game

> - Document your process
>   - How to write documentation
>   - How to write code
>   - How to contribute a feature
>   - How to write issues
>   - How to triage issues
>   - How to review a merge request
>   - How to make a release

::: notes

Write everything down. Even if it feels obvious; even if it's common
knowledge; even if it sounds incredibly stupid.

People don't know what they don't know, until they know it.

:::

---

## You get love for it

![Feedback for a release well done](nice.png){width=50%}

---

## You get love for it

> - Everyone loves you when things go according to plan
>   - Of course, you need a plan first
> - "Thanks for your patience"
> - Don't belittle people that use your software

::: notes

If you have a plan, people will follow it. It's the uncertainty that makes
people uneasy; they don't know how to contribute, they don't know how you will
handle bug reports, they don't know how to manage you.

Don't be passive; thank people for their patience, don't apologise for your
delay.

Don't be self-deprecating, or insult the thing you make, or the community you
are part of. People use your software, participate in your community; if you
belittle what you do, you're indirectly telling them that they chose an
inferior solution. Celebrate what you do, and the community you're in, even
when you have a clear view on their limits.

:::

---

## You get hate for it

![Linux kernel subsystem maintainer Hayao Miyazaki](miyazaki-free-software-mistake.png){width=50%}

::: notes

People will always hate stuff, even when it's offered to them freely and at
no cost.

People suck.

:::

---

## You get hate for it

 - People invest time and effort before asking you
 - Iteration *vs* Code drops
 - Scratching does not continue once the itch is gone
 - "Why can't you just merge this and fix it later?"
 - "You don't care about users because you won't merge my code"

::: notes

People who have a problem will invest time and effort, and inject their
personal feelings into what they do. You should not be surprised, after all
you do the same as a maintainer.

Once time and effort are spent, and a solution found that satisfies the
requirements of a person, it's still entirely possible for that solution to
fall short of your standards. Not everyone is willing to iterate once they
are satisfied; not everyone sees the long term issues of their solution; and
most people will always think their time is more precious than yours.

The important thing to learn is that you cannot logic them out of that. Nobody
writes code in a frictionless void; programming is not a logical endeavour.
That's the lie at the core of our profession.

Programming is deeply, and subtly emotional. It taps into a lot of places
that are core to your identity, personal history, memory, and context.
You cannot detach your code from who you are, as a person.

This means that you have to appeal to emotion, empathy, and sympathy; you have
to create a human connection.

:::

---

## You get nothing if you wait for it

 - Lack of triage
 - Lack of review
 - Lack of feedback
   - Even negative
 - Lack of **commitment**

::: notes

Don't make people wait too long for an initial answer. You don't have to
immediately context switch, and page everything out to a different
contribution; but you should at least give the indication that you are
reading things.

Triage is great, because at least it saves you time down the line to find
interesting things. Spend some time there. Tag issues. Use emoji buttons.

Show that somebody human is on the other side of the screen.

:::

---

## I wanna build something thatʼs gonna outlive me

> - Plan for you leaving the project
> - Take breaks, just to see what happens
> - Nominate lieutenants
> - Provide historical context and institutional knowledge
> - Remove yourself from the equation
> - If everything else fails, archive the repository

::: notes

Plan for the moment you decide to leave the project.

Guido van Rossum left his position of benevolent dictator of the Python
project after nearly 30 years.

You are not Guido. I can guarantee you that you're going to get bored, or
burned out, long before that.

Don't ghost people.

Plan ahead. Nobody will ever hold it against you if you decide to leave your
project, as long as you have an exit strategy.

:::

---

## If you stand for nothing, then what do you fall for?

 - Part of the ship, part of the crew
 - You get to share credit, you get to share blame
 - You will be seen as a representative, whether you like it or not

::: notes

You're part of GNOME. People will love you. People will hate you. People
will think you've done a ton of research for everything you do. People will
think you're an arrogant asshole that thinks they know better than everyone
else. People will not understand how much code you write. People will not
understand why you haven't left to work on a farm.

Most important of all: people will think you're part of a hive mind that
lives and breathes GNOME all day, every day, 365 days per year. You're
always on the clock, even, and especially, when we know that there's barely
an egg timer.

:::

---

## The art of the compromise

> - Involve to the engagement team when you have plans that might be controversial
> - Talk to the engagement team when you need help dealing the fallout
> - Get moderators to help you with bad faith actors

::: notes

Have somebody else speak for you. Don't be a hero, and make everything
worse, including your own mental health, in order to manage a public
relations situation.

:::

---

## Hold your nose and close your eyes

> - Stop going to hostile places
> - Stop trying to correct people on the Internet
> - Write down your position once, and then leave
> - Never, ever, *ever* engage with bad faith actors
> - Filter, ignore, block, lock down issues, and move on

::: notes

Correcting the Internet Peanut Gallery is not worth it. They will only waste
your time, because they know the value of nothing, and the cost of
absolutely nothing.

For them it's a low stakes game; for you, it's a one way ticket to an ulcer.

As I said before: software development is deeply personal.

:::

---

## We want our leaders to save the day

Maintainers:

 - **You** have the last word
   - That word may be "no" more often than it is "yes"
 - **Respect** different opinions, but don't be afraid to **dissent**
 - You don't owe **anything** to anybody using your project
 - You owe even less to anybody not using it

::: notes

"If you do this, I will move to your project"; "if you don't do this, I will
leave". This is emotional terrorism. Don't negotiate. Thank people for their
effort, then show them the door.

:::

---

## But we donʼt get a say in what they trade away

Users:

 - **Respect** the maintainer's opinion
 - They probably thought about this longer than you
 - They most definitely will be around longer than you

::: notes

:::

---

## We dream of a brand new start

![A perfectly valid option](only-sane-option.png){width=75%}

 - Downstream patches
 - Fork
 - Convince downstreams to pick up your fork

::: notes

It's perfectly fine to fork stuff. We use licenses that explicitly consider
this a right. We are part of a political, ethical, and social movement that
considers the right to take code, read it, and change it, as a fundamental
part of our life.

Don't be afraid of forks. If they are any good, somebody will use them for
their personal betterment; the users that pester you to change direction
will move there; users that look explicitly to what you provide will be
better served by your focus. Everybody wins.

If the fork isn't any good, it's not going to impact you anyway. The onus of
the fork is on the person that create the fork.

Keep a line open to the forks, and make sure you share fixes for issues in
the common code. It'll make everyone's life easier.

:::

---

## But we dream in the dark for the most part

 - Lots of effort
 - No guarantee of success
 - Novelty wears out fast
 - Now you get to maintain a whole project by yourself

::: notes

Forking is also a tough job. Forks will die, often even before they release
anything worth of value. The novelty wears out fast, and all you're left it
is a project to maintain; if you're not already used to it, it's going to
come crashing down on you like a ton of bricks.

When GNOME 2 got released, we had a "fork": four patches on top of a couple
of components, mostly to keep some pre-usability study change. You can't even
find the project page on the Wayback machine. Shout out to ma boi, oGalaxyo.

When GNOME 3 got released, we ended up with actual forks. This is good. Now,
people who liked GNOME 2 can go and use MATE, instead of trying to derail
GNOME's development process to get back to GNOME 2. It's great: everyone got
what they wanted.

But if you don't want that…

:::

---

## Dark as the tomb where it happens

 - Maybe better to get involved?
 - You may convince the maintainer
 - You may change your mind after looking at the problem from a different angle

::: notes

Nobody ever died because they changed idea on a piece of software. Don't
worry, you won't be the first.

:::

---

## Iʼve got to be in the room where it happens

![Something that never happened in the history of GNOME](no-drama.png){width=66%}

 - You will definitely learn something
 - Your opinions will carry more weight
 - You might end up being the next maintainer

::: notes

:::

---

## Click-boom

 - Automate **ALL** the things!
   - CI job for code style, static analysis, ASan/UBSan
 - Use more bots!
   - First triage, review requests, auto-closing
 - Use `CODEOWNERS`
   - It's great; and if GitLab used it to ping people for a review it
     would be even better *hint* *hint* *nudge* *nudge*
 - Mentor newcomers
   - Review trading, document decisions
 - Market your project better
   - Development blog (GTK, Shell), Discourse topics (Tracker)
 - Stop reading the comments
   - Dunning-Kruger in users leads to Brain Worms in maintainers

::: notes

There are lots of ways to improve this community, and the life and mental
health of the people maintaining software in it.

:::

---

# Thank you!

---

# Questions?
