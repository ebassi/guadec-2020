
This is my personal journey, and these are my personal recommendations. I
don't pretend to be the only source of truth in how to be a maintainer; all
I have to offer is 15 years of involvement in the community, a bunch of
software releases, some scars from flames, and a deep and long lasting
cynicism about software development, especially in free software.

Do what thou wilt with this knowledge.

If you've seen Jonathan Blandford's presentation in 2017 about the history
of GNOME, you will be familiar with GNOME's motto.

We all know that GNOME is like Soylent Green: it's made of people.

Sorry if I have spoiled the ending of a 47 years old movie; to be fair, the
trailer for the movie spoiled the ending as well. The '70s were weird, man.

Anyway.

Unlike Soylent Green, though, GNOME is also made of two other components:

- software; which is written, designed, documented, translated, and tested
  by the aforementioned "people"
- processes; which are followed by the same people in order to produce the
  software that can be consumed

So, if we put "people" at one end of the spectrum, and "software" at the
other, we get a nice little diagram on how to turn people into software.

We, justly, celebrate the people that make the software; and of course we
also celebrate our software in the form of releases; but the bits and blobs
in the middle are kind of obscure—often so obscure that people involved in
the GNOME project discover them long after they joined it.

The objective of this talk is to present the processes, including the bear
traps cleverly disguised in the tall grass, and possibly spurn people into
figuring out better processes—but, most importantly, to improve the
communication across the whole project.

---

Maintaining free software projects is not simple.

It typically involves a single person doing the body of work of the
equivalent of a team in the proprietary world: development, testing,
infrastructure, marketing, documentation writing, evangelism.

You can decide to stick to coding, but then you're just that: a developer.

You can involve different people, but then you become a team lead, and you
get to manage people more than your project.

Once you start involving other people, in any role, you'll need to have
access to high bandwidth/low latency communication channels. Anything else
will not scale, unless your project is very, very slow moving.

In GNOME we have a bunch of those venues: Discourse, chat, video
conferences. We also used to have hackfests and conferences, and maybe one
day we're going to go back to those.

Another thing you will need once you involve other people, is a way to
transfer knowledge from your head into theirs. As they say where I'm from,
nobody is born already learned. You had to learn, so don't be shocked if
somebody else needs to. We don't have good ways to transfer knowledge, in
the free software community. Sure, we can dump a bunch of files in a source
code repository, or write down some wiki page, but everything tends to go
out of date, for the same reason why writing a book against a free software
library is a sisyphean task: things change, often too fast, for
documentation to keep up. Even best practices change. Five years ago we
didn't have GitLab; maybe in 5 years time we're still going to use it, but
it's likely going to look and behave differently than today.

So all we have is training, in the form of mentorship. You, as a maintainer,
are responsible for teaching new contributors how to do your job, and bring
them up to speed. This, of course, means less time for coding.

In order to do that, GNOME offers a bunch of services to projects hosted on
our infrastructure. Don't try to reinvent your own; and don't try to go at it
alone.


